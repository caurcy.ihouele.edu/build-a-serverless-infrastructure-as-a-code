# Build a Serverless Infrastructure as a Code(IaC) using AWS

![image](https://miro.medium.com/v2/resize:fit:640/format:webp/1*B3EoqHHHO-3gUcNHyotjAg.png)

Créer un service RESTful en utilisant une fonction Lambda, API Gateway et des rôles IAM est nécessaire. Le service IAM est requis pour créer des rôles (permissions) permettant d'inviter une fonction lambda par elle-même et depuis API Gateway. Les modèles de formation Cloud Formation peuvent être créés de plusieurs manières, et cela dépend de la faisabilité du programmeur. Cependant, il est conseillé de suivre les meilleures pratiques d'AWS. Je vais utiliser la manière la plus simple de créer des ressources.



