## API Gateway creation:

Créer une passerelle API, une ressource, une méthode GET et activer CORS à travers cela. N'oubliez pas, après la création des méthodes dans les ressources, nous devons déployer l'API pour refléter les changements comme nous le faisons depuis la console ou le SDK.

Examiner le modèle complet avec toutes les parties. Cette stratégie est un peu difficile car elle contient plus de code, nous pouvons utiliser le modèle SAM pour réduire le code en utilisant la fonction de transformation dans CloudFormation. Nous examinerons le modèle SAM plus tard.