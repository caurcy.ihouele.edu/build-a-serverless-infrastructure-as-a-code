##  Lambda function creation:

Créer une fonction Lambda simple avec un message "Hello World". Nous utilisons la fonctionnalité de fichier zip dans la pile de formation CloudFormation pour télécharger le code. N'oubliez pas que nous utilisons la configuration AWS_PROXY pour la méthode API Gateway, donc nous devons absolument passer le statusCode et le body.